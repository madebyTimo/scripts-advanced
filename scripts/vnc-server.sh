#!/usr/bin/env bash
set -e

DEPENDENCIES=(x11vnc)
READ_WRITE=false
USER_PASSWORD=""

# Help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "Usage: $(basename "$0") [ARGUMENT]"
        echo "Starts a VNC server."
        echo "ARGUMENT can be"
        echo "    --password PASSWORD The password required to connect. Passwordless if empty"
        echo "    --read-write Allow remote control, not only viewing."
        exit
    fi
done

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# Check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--password" ]]; then
        shift
        USER_PASSWORD="$1"
    elif [[ "$1" == "--read-write" ]]; then
        READ_WRITE=true
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

ADDITIONAL_ARGUMENTS=()
if [[ "$READ_WRITE" != true ]]; then
    ADDITIONAL_ARGUMENTS+=(-viewonly)
fi
if [[ -n "$USER_PASSWORD" ]]; then
    ADDITIONAL_ARGUMENTS+=(-passwd "$USER_PASSWORD")
fi

x11vnc -forever -quiet -shared "${ADDITIONAL_ARGUMENTS[@]}"
