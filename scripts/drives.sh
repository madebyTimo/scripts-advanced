#!/usr/bin/env bash
set -e

CALLER_DIR="${PWD}"
DEPENDENCIES=(mkfs.fat mke2fs parted tune2fs)
MODE=""
NAME=""
RESSOURCE_PATH=""
RESERVED_BLOCK_COUNT="250000"
SIZE=("0%" "100%")
TYPE=""
WORKING_DIR="${PWD}/.temp-$(basename "$0")"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") [ARGUMENT]"
        echo "Format drives and create partitions."
        echo "ARGUMENT can be"
        echo "    --create-partition [ext4|fat32|ntfs] DRIVE Create partition."
        echo "    --delete-partition PARTITION Delete specified partition."
        echo "    --format-drive [gpt|msdos] DRIVE Format disk."
        echo "    --name NAME Label of partition to create."
        echo "    --set-reserved-blocks PARTITION Set reserved blocks to $RESERVED_BLOCK_COUNT."
        echo "    --size START END Specify size of partition to create, default: \"${SIZE[*]}\"."
        exit
    fi
done

# check if run as root
if [[ "$(id --user)" != 0 ]]; then
    echo "Script must run as root"
    if [[ -n "$(which sudo)" ]];then
        echo "Try with sudo"
        sudo "$0" "$@"
        exit
    fi
    exit 1
fi

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which $CMD)" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--create-partition" ]]; then
        MODE="create-partition"
        shift
        TYPE="$1"
        shift
        RESSOURCE_PATH="$1"
    elif [[ "$1" == "--delete-partition" ]]; then
        MODE="delete-partition"
        shift
        RESSOURCE_PATH="$1"
    elif [[ "$1" == "--format-drive" ]]; then
        MODE="format-drive"
        shift
        TYPE="$1"
        shift
        RESSOURCE_PATH="$1"
    elif [[ "$1" == "--name" ]]; then
        shift
        NAME="$1"
    elif [[ "$1" == "--set-reserved-blocks" ]]; then
        MODE="set-reserved-blocks"
        shift
        RESSOURCE_PATH="$1"
    elif [[ "$1" == "--size" ]]; then
        shift
        SIZE=("$1")
        shift
        SIZE+=("$1")
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

if [[ "$MODE" == "create-partition" ]]; then
    OLD_ONES="$(echo "$RESSOURCE_PATH"*)"
    parted --align optimal "$RESSOURCE_PATH" mkpart primary "$TYPE" "${SIZE[@]}"
    sleep 10
    RESSOURCE_PATH="$(echo "$RESSOURCE_PATH"*) "
    for ELEMENT in $OLD_ONES; do
        RESSOURCE_PATH="${RESSOURCE_PATH//"$ELEMENT "/}"
    done
    RESSOURCE_PATH="${RESSOURCE_PATH//" "/}"
    echo "new partition is \"${RESSOURCE_PATH}\""
    if [[ "$TYPE" == ext* ]]; then
        mke2fs -t "$TYPE" "$RESSOURCE_PATH" -L "$NAME"
    elif [[ "$TYPE" == fat* ]]; then
        mkfs.fat -F "${TYPE#fat}" "$RESSOURCE_PATH" -n "$NAME"
    fi
elif [[ "$MODE" == "delete-partition" ]];then
    RESSOURCE_PATH_DEVICE="${RESSOURCE_PATH%%[0-9]*}"
    parted "$RESSOURCE_PATH_DEVICE" rm "${RESSOURCE_PATH#"$RESSOURCE_PATH_DEVICE"}"
elif [[ "$MODE" == "format-drive" ]];then
    parted "$RESSOURCE_PATH" mklabel "$TYPE"
elif [[ "$MODE" == "set-reserved-blocks" ]]; then
    tune2fs -r "$RESERVED_BLOCK_COUNT" "$RESSOURCE_PATH"
fi
