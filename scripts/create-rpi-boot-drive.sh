#!/usr/bin/env bash
set -e

CREDENTIALS="piuser:0708"
DEPENDENCIES=(drives.sh xz)
DRIVE="/dev/null"
HOSTNAME="RPi"
INIT_SCRIPT=""
PARTITION_TYPE="gpt"
WORKING_DIR="/tmp/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") [ARGUMENT]"
        echo "Create a gpt ready-to-boot drive for Raspberry Pi."
        echo "ARGUMENT can be"
        echo "    --credentials CREDENTIALS initial user and password, default: \"${CREDENTIALS}\""
        echo "    --drive DRIVE the drive to write on"
        echo "    --hostname HOSTNAME hostname to configure, default: \"${HOSTNAME}\""
        echo "    --init-script PATH init script to run at first boot"
        echo "    --partition-type [gpt|msdos] partition type to use," \
            "default: \"${PARTITION_TYPE}\""
        exit
    fi
done

# check if run as root
if [[ "$(id --user)" != 0 ]]; then
    echo "Script must run as root"
    if [[ -n "$(which sudo)" ]];then
        echo "Try with sudo"
        sudo "$0" "$@"
        exit
    fi
    exit 1
fi

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

function cleanup {
    umount "$WORKING_DIR/driveMount/boot" "$WORKING_DIR/driveMount/rootfs" \
        "$WORKING_DIR/imageMount/boot" "$WORKING_DIR/imageMount/rootfs" || true
    rm -f -r "$WORKING_DIR"
}
trap cleanup EXIT

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--credentials" ]]; then
        shift
        CREDENTIALS="$1"
    elif [[ "$1" == "--drive" ]]; then
        shift
        DRIVE="$1"
    elif [[ "$1" == "--hostname" ]]; then
        shift
        HOSTNAME="$1"
    elif [[ "$1" == "--init-script" ]]; then
        shift
        INIT_SCRIPT="$(realpath "$1")"
    elif [[ "$1" == "--partition-type" ]]; then
        shift
        PARTITION_TYPE="$1"
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

IMAGE_DATE="$(curl --silent --location \
        "https://downloads.raspberrypi.com/raspios_lite_arm64/release_notes.txt" | \
    head --lines 1 | \
    grep --extended-regexp --only-matching '[0-9]{4}-[0-9]{2}-[0-9]{2}')"
IMAGE_URL="https://downloads.raspberrypi.com/raspios_lite_arm64/images/raspios_lite_arm64-${IMAGE_DATE}"
IMAGE_URL+="/$(curl --silent --location "$IMAGE_URL" | \
    grep --extended-regexp --only-matching \
        "${IMAGE_DATE}-raspios-[a-z]+-arm64-lite.img.xz" | \
    head --lines 1)"

echo "Download Raspberry Pi OS image from \"${IMAGE_DATE}\""
curl --silent --location "$IMAGE_URL" |
    xz --decompress > image.img
OFFSET_BOOT=""
SECTORS_BOOT=""
mapfile -t LINES < <(fdisk -l "image.img" | grep "image.img1" | grep -v "Disk image.img")
for LINE in "${LINES[@]}"; do
    OFFSET_BOOT=$(echo $LINE | sed 's|\*||' | awk '{print $2}')
    SECTORS_BOOT=$(echo $LINE | sed 's|\*||' | awk '{print $4}')
    break
done
OFFSET_ROOTFS=""
SECTORS_ROOTFS=""
mapfile -t LINES < <(fdisk -l "image.img" | grep "image.img2" | grep -v "Disk image.img")
for LINE in "${LINES[@]}"; do
    OFFSET_ROOTFS=$(echo $LINE | sed 's|\*||' | awk '{print $2}')
    SECTORS_ROOTFS=$(echo $LINE | sed 's|\*||' | awk '{print $4}')
    break
done
mkdir imageMount imageMount/boot imageMount/rootfs
mount -o loop,offset=$(($OFFSET_BOOT * 512)),sizelimit=$(($SECTORS_BOOT * 512)) --read-only \
    "${WORKING_DIR}/image.img" "${WORKING_DIR}/imageMount/boot"
mount -o loop,offset=$(($OFFSET_ROOTFS * 512)),sizelimit=$(($SECTORS_ROOTFS * 512)) --read-only \
    "${WORKING_DIR}/image.img" "${WORKING_DIR}/imageMount/rootfs"

echo "Format drive \"${DRIVE}\""
drives.sh --format-drive "$PARTITION_TYPE" "$DRIVE"
drives.sh --create-partition fat32 "$DRIVE" --name boot --size 0% 500M
drives.sh --create-partition ext4 "$DRIVE" --name root --size 500M 100%
mkdir driveMount driveMount/boot driveMount/rootfs
mount "${DRIVE}"?1  "${WORKING_DIR}/driveMount/boot"
mount "${DRIVE}"?2  "${WORKING_DIR}/driveMount/rootfs"

echo "Copy files to drive"
cp --archive --no-target-directory -r imageMount/boot driveMount/boot
cp --archive --no-target-directory -r imageMount/rootfs driveMount/rootfs

PART_UUID_BOOT="$(blkid --match-tag PARTUUID --output value  "${DRIVE}"?1)"
PART_UUID_ROOTFS="$(blkid --match-tag PARTUUID --output value  "${DRIVE}"?2)"
sed --in-place \
    --expression "s|\(root=PARTUUID=\)[^ ]*\( \)|\1${PART_UUID_ROOTFS}\2|" \
    --expression "s| init=/usr/lib/raspberrypi-sys-mods/firstboot||" \
    driveMount/boot/cmdline.txt
sed --in-place \
    --expression "s|\(PARTUUID=\)[^ ]*\([ ]*/boot/firmware \)|\1${PART_UUID_BOOT}\2|" \
    --expression "s|\(PARTUUID=\)[^ ]*\([ ]*/ \)|\1${PART_UUID_ROOTFS}\2|" \
    driveMount/rootfs/etc/fstab

echo "Set configuration"
echo "${CREDENTIALS%%':'*}:$(openssl passwd -6 "${CREDENTIALS#*':'}")" \
    > driveMount/boot/userconf.txt
echo "$HOSTNAME" > driveMount/rootfs/etc/hostname
sudo sed --in-place \
    --expression 's|CONF_SWAPSIZE=[0-9]\+|CONF_SWAPSIZE=10000|' \
    --expression 's|#\?CONF_MAXSWAP=[0-9]\+|CONF_MAXSWAP=10000|' \
    driveMount/rootfs/etc/dphys-swapfile
sed --in-place 's|^exit 0|/init-main.sh \&\nexit 0|' driveMount/rootfs/etc/rc.local
cat << EOF > driveMount/rootfs/init-main.sh
#!/bin/bash
set -e
if [[ -f /init.sh ]]; then
    /init.sh
    rm /init.sh
fi
sed --in-place '/\/init-main.sh \&/d' /etc/rc.local
rm "\$0"
EOF
chmod +x driveMount/rootfs/init-main.sh
if [[ -f "$INIT_SCRIPT" ]]; then
    cp "$INIT_SCRIPT" driveMount/rootfs/init.sh
    chmod +x driveMount/rootfs/init.sh
fi
