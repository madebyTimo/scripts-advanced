#!/usr/bin/env bash
set -e

CALLER_DIR="${PWD}"
DEPENDENCIES=(download.sh gzip xorriso)
ISO_URL="https://deb.debian.org/debian/dists/stable/main/installer-amd64/current/images/netboot/\
mini.iso"
PRESEED_FILE=""
TARGET_FILENAME=debian-preseeded-$(date +"%Y.%m.%d").iso
WORKING_DIR="${PWD}/.temp-$(basename "$0")"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") [ARGUMENT]"
        echo "Create a debian installer image which contains the preseed file."
        echo "ARGUMENT can be"
        echo "    --preseed FILE the preseed file."
        exit
    fi
done

# check if run as root
if [[ "$(id --user)" != 0 ]]; then
    echo "Script must run as root"
    if [[ -n "$(which sudo)" ]];then
        echo "Try with sudo"
        sudo "$0" "$@"
        exit
    fi
    exit 1
fi

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--preseed" ]]; then
        shift
        PRESEED_FILE="$(realpath "$1")"
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

if [[ ! -f "$PRESEED_FILE" ]]; then
    echo "Preseed file \"${PRESEED_FILE}\" not found."
    exit 1
fi

if [[ -e "$WORKING_DIR" ]]; then
    echo "\"${WORKING_DIR}\" already exists. Remove in 10 seconds."
    sleep 10
    rm -f -r "$WORKING_DIR"
fi
mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

echo "download iso"
download.sh --name image.iso "$ISO_URL"
mkdir isofiles
xorriso -indev image.iso -osirrox on -report_about WARNING -extract / isofiles > /dev/null

echo "insert preseed"
cp "$PRESEED_FILE" "preseed.cfg"
gunzip "isofiles/initrd.gz"
echo preseed.cfg | cpio --append --create --file isofiles/initrd --format newc
gzip "isofiles/initrd"
find isofiles -follow -type f ! -name md5sum.txt -print0 | xargs -0 md5sum > isofiles/md5sum.txt

echo "repack iso"
dd if="image.iso" bs=1 count=432 of="mbrTemplate"
xorriso -report_about WARNING -as mkisofs \
   -r -V "debian preseeded $(date +"%Y.%m.%d")" \
   -o "${CALLER_DIR}/$TARGET_FILENAME" \
   -J -J -joliet-long \
   -isohybrid-mbr mbrTemplate \
   -b isolinux.bin \
   -c boot.cat \
   -boot-load-size 4 -boot-info-table -no-emul-boot \
   -eltorito-alt-boot \
   -e boot/grub/efi.img \
   -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus \
   isofiles/

rm -f -r "$WORKING_DIR"
