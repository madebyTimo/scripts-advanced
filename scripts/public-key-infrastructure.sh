#!/usr/bin/env bash
set -e

CA_CSR=false
CALLER_DIR="${PWD}"
CERTIFICATE=""
CERTIFICATE_AUTHORITY=""
CERTIFICATE_SIGNING_REQUEST=""
DOMAIN=""
KEY=""
MODE=""
DEPENDENCIES=(openssl ssh-keygen)
SUBJECT=""
TYPE=""
VALIDITY_TIME="365"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") ARGUMENT..."
        echo "Does some things with certificates."
        echo "ARGUMENT can be"
        echo "    --ca CA The certificate authority to use."
        echo "    --create ca|cert|csr|key|ssh-key Create the requested file."
        echo "    --csr CSR The certificate signing request to sign."
        echo "    --domain DOMAIN The domain to create the certificate for."
        echo "    --key KEY The key to use."
        echo "    --subject SUBJECT The subject to for the certificate."
        echo "    --validity-time DAYS Validity in days, default \"${VALIDITY_TIME}\"."
        echo "    --verify CERTIFICATE Verify the certificate."
        echo "    --view CERTIFICATE View certificate information."
        echo "examples:"
        echo "create key:"
        echo "    $(basename "$0") --create key"
        echo "create self signed certificate"
        echo "    $(basename "$0") --create cert --key key.pem --subject" \
            "\"/countryName=DE/organizationName=orga/commonName=example.madebytimo.de/\
emailAddress=mail@example.madebytimo.de\""
        echo "create csr:"
        echo "    $(basename "$0") --create csr --key key.pem --subject" \
            "\"/commonName=example.madebytimo.de\""
        echo "create cert from csr:"
        echo "    $(basename "$0") --create cert --ca ca.pem --csr cert.csr --key ca-key.pem"
        echo "verify cert.pem is signed by ca.pem:"
        echo "    $(basename "$0") --verify cert.pem --ca ca.pem"
        exit
    fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--ca" ]]; then
        shift
        CERTIFICATE_AUTHORITY="$1"
    elif [[ "$1" == "--create" ]]; then
        MODE="create"
        shift
        TYPE="$1"
    elif [[ "$1" == "--domain" ]]; then
        shift
        DOMAIN="$1"
    elif [[ "$1" == "--key" ]]; then
        shift
        KEY="$1"
    elif [[ "$1" == "--csr" ]]; then
        shift
        CERTIFICATE_SIGNING_REQUEST="$1"
    elif [[ "$1" == "--subject" ]]; then
        shift
        SUBJECT="$1"
    elif [[ "$1" == "--validity-time" ]]; then
        shift
        VALIDITY_TIME="$1"
    elif [[ "$1" == "--verify" ]]; then
        MODE="verify"
        shift
        CERTIFICATE="$1"
    elif [[ "$1" == "--view" ]]; then
        MODE="view"
        shift
        CERTIFICATE="$1"
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

checkCertificate(){
    if [[ ! -f "$CERTIFICATE" ]]; then
        echo "Certificate \"${CERTIFICATE}\" not found"
        exit 1
    fi
}

checkCertificateAuthority(){
    if [[ ! -f "$CERTIFICATE_AUTHORITY" ]]; then
        echo "Certificate authority \"${CERTIFICATE_AUTHORITY}\" not found"
        exit 1
    fi
}
checkCertificateSigningRequest(){
    if [[ ! -f "$CERTIFICATE_SIGNING_REQUEST" ]]; then
        echo "Certificate signing request \"${CERTIFICATE_SIGNING_REQUEST}\" not found"
        exit 1
    fi
}
checkDomain(){
    if [[ -z "$DOMAIN" ]]; then
        echo "Domain not specified."
        exit 1
    fi
}
checkKey(){
    if [[ ! -f "$KEY" ]]; then
        echo "Key \"${KEY}\" not found"
        exit 1
    fi
}
checkSubject(){
    if [[ -z "$SUBJECT" ]]; then
        echo "Caution! Subject is empty."
    fi
}

if [[ "$MODE" == "create" ]]; then
    if [[ "$TYPE" == @(ca|cert) ]]; then
        checkDomain
        checkKey
        ADDITIONAL_ARGUMENTS=()
        if (checkCertificateSigningRequest); then
            echo "create certificate authority signed certificate"
            checkCertificateAuthority
            ADDITIONAL_ARGUMENTS+=(-CA "$CERTIFICATE_AUTHORITY" -CAkey "$KEY")
        else
            echo "create self signed certificate"
            $0 --create csr --key "$KEY" --subject "$SUBJECT"
            CERTIFICATE_SIGNING_REQUEST="${CALLER_DIR}/csr.pem"
            ADDITIONAL_ARGUMENTS+=(-key "$KEY")
        fi
        X509_EXTENSIONS="
            basicConstraints=critical,CA:$([[ $TYPE == ca ]] && echo TRUE || echo FALSE)
            subjectAltName=DNS:$DOMAIN
        "
        openssl x509 -req -in "$CERTIFICATE_SIGNING_REQUEST" -out "${CALLER_DIR}/cert.pem" \
            -days "$VALIDITY_TIME" -extfile <(echo "$X509_EXTENSIONS") "${ADDITIONAL_ARGUMENTS[@]}"
    elif [[ "$TYPE" == "csr" ]]; then
        echo "create certificate signing request"
        checkKey
        checkSubject
        openssl req -new -key "$KEY" -out "${CALLER_DIR}/csr.pem" \
            -subj "${SUBJECT:-"/commonName=$DOMAIN"}"
    elif [[ "$TYPE" == "key" ]]; then
        openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:secp521r1 \
            -out "${CALLER_DIR}/key.pem"
    elif [[ "$TYPE" == "ssh-key" ]]; then
        ssh-keygen -t ed25519 -N "" -C "$SUBJECT" -f "${CALLER_DIR}/ssh-key"
    fi
elif [[ "$MODE" == "verify" ]]; then
    echo "verify signature"
    checkCertificate
    ADDITIONAL_ARGUMENTS=()
    if (checkCertificateAuthority); then
        ADDITIONAL_ARGUMENTS+=(-CAfile "$CERTIFICATE_AUTHORITY")
    fi
    openssl verify "${ADDITIONAL_ARGUMENTS[@]}" "$CERTIFICATE"
elif [[ "$MODE" == "view" ]]; then
    echo "view certificate"
    checkCertificate
    if grep --silent "^-----BEGIN CERTIFICATE REQUEST-----$" "$CERTIFICATE"; then
        openssl req -text -noout -in "$CERTIFICATE"
    else
        openssl x509 -text -noout -in "$CERTIFICATE"
    fi
else
    echo "No mode selected"
    exit 1
fi
