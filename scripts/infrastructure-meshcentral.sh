#!/usr/bin/env bash
set -e

COMMAND=()
DEPENDENCIES=(docker)
MESHCENTRAL_PASSWORD=""
MESHCENTRAL_URL=""
MESHCENTRAL_USERNAME=""
WORKDIR=""

# Help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "Usage: $(basename "$0") [ARGUMENT] COMMAND"
        echo "Runs the COMMAND in a docker container with a meshcentral port of" \
            "the infrastructure scripts."
        echo "For more details see" \
            "https://github.com/mbT-Infrastructure/docker-scripts-infrastructure-meshcentral"
        echo "ARGUMENT can be"
        echo "    --meshcentral-password PASSWORD Password to use for the Meshcentral connection."
        echo "    --meshcentral-url URL Url of the Meshcentral server."
        echo "    --meshcentral-user USERNAME Username to use for the Meshcentral connection."
        echo "    --mount-workdir DIR Mount a dir as workdir into the container to reference files."
        exit
    fi
done

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# Check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--meshcentral-password" ]]; then
        shift
        MESHCENTRAL_PASSWORD="$1"
    elif [[ "$1" == "--meshcentral-url" ]]; then
        shift
        MESHCENTRAL_URL="$1"
    elif [[ "$1" == "--meshcentral-user" ]]; then
        shift
        MESHCENTRAL_USERNAME="$1"
    elif [[ "$1" == "--mount-workdir" ]]; then
        shift
        WORKDIR="$1"
    else
        COMMAND+=("$1")
    fi
    shift
done

if  [[ -z "$MESHCENTRAL_PASSWORD" ]] || [[ -z "$MESHCENTRAL_URL" ]] \
        || [[ -z "$MESHCENTRAL_USERNAME" ]]; then
    echo "Please set url, username and password of Meshcentral."
    exit 1
fi


WORKDIR_MOUNT=()
if [[ -n "$WORKDIR" ]]; then
    WORKDIR_MOUNT+=(--volume "$(realpath "$WORKDIR"):/media/workdir")
fi

docker run --pull always --rm \
    --env "SERVER_PASSWORD=$MESHCENTRAL_PASSWORD" \
    --env "SERVER_URL=$MESHCENTRAL_URL" \
    --env "SERVER_USERNAME=$MESHCENTRAL_USERNAME" \
    --env "SKIP_REBOOT=$SKIP_REBOOT" \
    "${WORKDIR_MOUNT[@]}" \
    madebytimo/scripts-infrastructure-meshcentral "${COMMAND[@]}"
